module Main exposing (main)

import Browser
import Html exposing (Html, text)

import Graphql.Operation exposing (RootQuery)
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet)
import Api.Object
import Api.Object.User as User
import Api.Query as Query
import Api.Mutation as Mutation
import Api.Scalar exposing (Id(..))

-- MAIN

main =
  Browser.element
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

-- MODEL

type alias Model
  = { accessToken : String
    , user : Maybe User
    }

type alias UserData =
  { id : String
  , name : String
  , contact : String
  , queue : QueueData
  , experience : List ExperienceData
  , goals : List String
  , paired : Maybe User
  }
