-- Do not manually edit this file, it was auto-generated by dillonkearns/elm-graphql
-- https://github.com/dillonkearns/elm-graphql
module Api.Query exposing (..)

import Graphql.Internal.Builder.Argument as Argument exposing (Argument)
import Graphql.Internal.Builder.Object as Object
import Graphql.Internal.Encode as Encode exposing (Value)
import Graphql.Operation exposing (RootMutation, RootQuery, RootSubscription)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet exposing (SelectionSet)
import Json.Decode as Decode exposing (Decoder)
import Api.Object
import Api.Interface
import Api.Union
import Api.Scalar
import Api.InputObject
import Api.ScalarCodecs
import Graphql.Internal.Builder.Object as Object
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet exposing (SelectionSet)
import Graphql.Operation exposing (RootMutation, RootQuery, RootSubscription)
import Json.Decode as Decode exposing (Decoder)
import Graphql.Internal.Encode as Encode exposing (Value)

me : SelectionSet decodesTo Api.Object.User
 -> SelectionSet (Maybe decodesTo) RootQuery
me object____ =
      Object.selectionForCompositeField "me" [] (object____) (Basics.identity >> Decode.nullable)


queue : SelectionSet decodesTo Api.Object.Queue
 -> SelectionSet (Maybe decodesTo) RootQuery
queue object____ =
      Object.selectionForCompositeField "queue" [] (object____) (Basics.identity >> Decode.nullable)


type alias UserRequiredArguments = { id : Api.ScalarCodecs.Id }

user : UserRequiredArguments
 -> SelectionSet decodesTo Api.Object.User
 -> SelectionSet (Maybe decodesTo) RootQuery
user requiredArgs____ object____ =
      Object.selectionForCompositeField "user" [ Argument.required "id" requiredArgs____.id ((Api.ScalarCodecs.codecs |> Api.Scalar.unwrapEncoder .codecId)) ] (object____) (Basics.identity >> Decode.nullable)
