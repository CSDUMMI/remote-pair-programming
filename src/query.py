from flask import g
from ariadne import ObjectType
import os
import database as db
from pair_users import pair_users

ACCESS_TOKEN_TIMEOUT = os.environ["ACCESS_TOKEN_TIMEOUT"]

query = ObjectType("Query")

@query.field("me")
def resolve_me(_, info):
    return db.User.get_by_id(info.context["id"])


@query.field("queue")
def resolve_queue(_, info):
    user = db.User.get_by_id(info.context["id"])
    return user.queue


@query.field("user")
def resolve_user(_, info, id):
    try:
        user = db.User.get_by_id(id)
        if id != info.context["id"]:
            user.contact = None

        return user
    except Exception as e:
        return None
