from ariadne import ObjectType
import time
from pair_users import pair_users
import database as db



mutation = ObjectType("Mutation")

@mutation.field("enterQueue")
def resolve_enterQueue(_, info, mode):
    user = db.User.get_by_id(info.context["id"])

    paired_user = pair_users(user, mode)

    if paired_user:
        user.paired = paired_user
        paired_user.paired = user

        paired_user.save()
    else:
        user.queue = db.Queue.create(mode = mode, time = time.time())
        print(user.queue)

        user.save()

    return user.queue

@mutation.field("exitQueue")
def resolve_exitQueue(_, info):
    user = db.User.get_by_id(info.context["id"])

    if user.queue is not None:
        user.queue = None

    user.pairedWith = None

    user.save()

    return True

@mutation.field("changeName")
def resolve_changeName(_, info, name):
    user = db.User.get_by_id(info.context["id"])

    if not db.User.select().where(db.User.name == name).exists():
        user.name = name

        user.save()

    return user

@mutation.field("addLanguage")
def resolve_addLanguage(_, info, lang):
    user = db.User.get_by_id(info.context["id"])

    if lang not in user.languages:
        user.languages.add(lang)
        user.save()

    print(user)
    return user

@mutation.field("removeLanguage")
def resolve_removeLanguage(_, info, lang):
    user = db.User.get_by_id(info.context["id"])

    if lang in user.languages:
        user.languages.remove(lang)
        user.save()

    return user

@mutation.field("adjustExp")
def resolve_adjustExp(_, info, topic, exp):
    user = db.User.get_by_id(info.context["id"])

    found = False
    for experience in user.experience:
        if experience.topic == topic:
            experience.exp = exp
            experience.save()

            found = True
            break

    if not found:
        db.Experience.create(
            topic = topic,
            user = user,
            exp = exp,
        )

    return user
