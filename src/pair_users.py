import database as db

def pair_users(user, mode):
    users = list(db.User.select().where(db.User.id != user.id).where(db.User.queue.is_null(True)))

    users = filter(lambda u:
                    (u.queue.mode == mode
                    or (mode == "TUTORING" and u.mode == "LEARNING")
                    or (mode == "LEARNING" and u.mode == "TUTORING")),
                    users)

    users = filter(lambda u: u.languages & user.languages != set(), users)

    users = sorted(users, key = lambda u: u.queue.time, reverse = True)
    
    users = sorted(users, key = lambda u: goalSum(u, user))

    users = sorted(users, key = lambda u: goalSum(user, u))

    users = list(reversed(users))

    if len(users) != 0 and goalSum(user, users[0])/goalSum(users[0], user) >= 0.5:
        return users[0]
    else:
        return False

def goalSum(user1, user2):
    """How experienced is user1 on the goals of user2
    """
    return sum([exp.exp for exp in user1.experience if exp.topic in user2.goals])
