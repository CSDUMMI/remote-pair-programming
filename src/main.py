import os
import uuid
from flask import Flask, request, jsonify, g, render_template
from Crypto.Hash import SHA256
from Crypto.Random import get_random_bytes
from ariadne import gql, make_executable_schema, load_schema_from_path, graphql_sync
from ariadne.constants import PLAYGROUND_HTML

import time
from query import query
from mutation import mutation
import database as db

ACCESS_TOKEN_TIMEOUT = float(os.environ["ACCESS_TOKEN_TIMEOUT"])

schema = make_executable_schema(load_schema_from_path("../schema.graphql"), query, mutation)

app = Flask(__name__,
            template_folder = "../templates",
            static_folder = "../static",
            static_url_path = "/static")

if not db.User.select().where(db.User.name == "admin").exists():
    password_salt = get_random_bytes(32)
    password_hash = SHA256.new(data = "root".encode("utf-8") + password_salt).digest()

    db.User.create(
        id = uuid.uuid4(),
        name = "admin",
        contact = "mastodon:@csddumi@norden.social",
        password_hash = password_hash,
        password_salt = password_salt,
        queue = db.Queue.create(
            time = time.time(),
            mode = "LEARNING",
        )
    )

accessTokens = {}
accessTokens["testToken"] = {
    "time": time.time(),
    "id": db.User.get(db.User.name == "admin"),
}

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/login", methods=["POST"])
def login():
    body = request.json()
    name = body["name"]
    password = body["password"]

    user = db.User.get(db.User.name == name | db.User.contact == db.User.contact)

    if user.password_hash == SHA256.new(data = password.encode("utf-8") + user.password_salt).digest():
        accessToken = get_random_bytes(16).decode("utf-8")
        accessTokens[accessToken] = {
            "time": time.time(),
            "id": user.id,
        }
        return accessToken, 200
    else:
        return "401 Unauthorized", 401

@app.route("/signup", methods=["POST"])
def signup():
    body = request.json()
    name = body["name"]
    password = body["password"]
    languages = body.get("languages", default = ["eng", "deu"])

    password_salt = generate_random_bytes(32)

    password_hash = SHA256.new(password.encode("utf-8") + password_salt).digest()

    user = db.User.create(
        id = uuid.uuid4(),
        name = name,
        languages = languages,
        password_hash = password_hash,
        password_salt = password_salt,
    )

    return "signed up", 200

@app.route("/graphql", methods = ["GET", "POST"])
def graphql():
    if request.method == "POST":

        if request.headers.get("Authorization") is None:
            return "401 Unauthorized", 401

        accessToken = request.headers.get("Authorization").split(" ")[1]

        if accessTokens[accessToken]["time"] - time.time() > ACCESS_TOKEN_TIMEOUT:
            return "401 Unauthorized", 401

        data = request.get_json()

        success, result = graphql_sync(
            schema,
            data,
            context_value={
                "request": request,
                "id": accessTokens[accessToken]["id"],
            },
            debug=app.debug

        return jsonify(result), status_code
    else:
        return PLAYGROUND_HTML, 200
