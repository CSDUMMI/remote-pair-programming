import peewee as pw
import peeweedbevolve
import os

from Crypto.Hash import SHA256
from Crypto.Random import get_random_bytes

DATABASE = os.environ["DATABASE"]

database = pw.SqliteDatabase(DATABASE)

class BaseModel(pw.Model):
    class Meta:
        database = database

class LanguagesField(pw.Field):
    field_type = "text"

    def db_value(self, value):
        return ",".join(value)

    def python_value(self, value):
        return set(value.split(","))

class Queue(BaseModel):
    time = pw.FloatField()
    mode = pw.CharField()

class User(BaseModel):
    id = pw.UUIDField(primary_key = True)
    name = pw.TextField(unique = True)
    contact = pw.TextField(unique = True)
    languages = LanguagesField(default = set(["eng","deu"]))

    password_hash = pw.BlobField()
    password_salt = pw.BlobField()

    queue = pw.ForeignKeyField(Queue, null = True)
    paired = pw.DeferredForeignKey("User", deferrable='INITIALLY DEFERRED', null = True)

class Experience(BaseModel):
    topic = pw.CharField()
    user = pw.ForeignKeyField(User, backref = "experience")
    exp = pw.IntegerField()

def create_tables():
    with database:
        database.create_tables([User, Experience, Queue])

create_tables()
